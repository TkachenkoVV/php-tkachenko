<?php

/* 
 * Користувач вводить число від -200 до 200. 
 * Потрібно вивести на екран стрічкове представлення введеного значення. 
 * Наприклад:
 * 
 * 0 => “нуль”
 * -1 => “мінус один”
 * 143 => “сто сорок три”
 * -199 => “мінус сто дев’яносто дев’ять”
 * 
 * Якщо введене значення не є числом або не входить до вказаного діапазону, 
 * потрібно виводити повідомлення про помилку.

 */

//Зробити змінні під кожен розряд!!

//Зчитуємо та перевіряємо введене число

echo "Введіть число між -200 та 200: ";
fscanf(STDIN, "%d", $number);

if ($number < -200 || $number > 200) {
    exit ("Потрібно ввести число між -200 та 200.\n");
}

$result = '';

if ($number == 0) {
    $result = "нуль";
    exit();

//Розбираємо число порозрядно 
if($number < 0) {
    $result .= "мінус ";
    $number *= -1;
}

$hundreds = (integer)($number / 100);
$tens = (integer)($number % 100 / 10);
$ones = (integer)($number % 10);

} else {
    switch ($hundreds) {
    case 1: $result .= "сто";
        break;
    case 2: $result .= "двісті";
        break;
    }
    
    switch ($tens) {
    case 1: 
        switch ($ones) {
        case 0: $result .= "десять";
            break;
        case 1: $result .= "одинадцять";
            break;
        case 2: $result .= "дванадцять";
            break;
        case 3: $result .= "тринадцять";
            break;
        case 4: $result .= "чотирнадцять";
            break;
        case 5: $result .= "п'ятнадцять";
            break;
        case 6: $result .= "шістнадцять";
            break;
        case 7: $result .= "сімнадцять";
            break;
        case 8: $result .= "вісімнадцять";
            break;
        case 9: $result .= "дев'ятнадцять";
            break;
        }
        break;
    case 2: $result .= "двадцять";
        break;
    case 3: $result .= "тридцять ";
        break;
    case 4: $result .= "сорок ";
        break;
    case 5: $result .= "п'ятдесят ";
        break;
    case 6: $result .= "шістдесят ";
        break;
    case 7: $result .= "сімдесят ";
        break;
    case 8: $result .= "вісімдесят ";
        break;
    case 9: $result .= "дев'яносто ";
        break;
    }
    
    if ($tens != 1) {
        switch ($ones) {
        case 1: $result .= "один";
            break;
        case 2: $result .= "два";
            break;
        case 3: $result .= "три";
            break;
        case 4: $result .= "чотири";
            break;
        case 5: $result .= "п'ять";
            break;
        case 6: $result .= "шість";
            break;
        case 7: $result .= "сім";
            break;
        case 8: $result .= "вісім";
            break;
        case 9: $result .= "дев'ять";
            break;
        }
    }
}

echo "$result\n";

