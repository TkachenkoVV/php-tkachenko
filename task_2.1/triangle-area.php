<?php

/* 
 * Написати консольний скрипт, який приймає від користувача три параметри - 
 * довжини сторін прямокутного трикутника в метрах, і виводить його площу в м2. 
 * У випадку, якщо користувач вводить некоректні значення параметрів (нечислові,
 * від’ємні і т.д.), потрібно виводити повідомлення про помилку.
 * 
 * Обчислюється площа трикутника по наявним трьом сторонам
 */

//Зчитування сторін трикутника і валідація
echo "Pls enter length of first side: ";
fscanf(STDIN, "%d\n", $triangleSide1);
if ($triangleSide1 < 1) {
    exit('Length cant be less then 1');
}

echo "Pls enter length of second side: ";
fscanf(STDIN, "%d\n", $triangleSide2);
if ($triangleSide2 < 1) {
    exit('Length cant be less then 1');
}

echo "Pls enter length of third side: ";
fscanf(STDIN, "%d\n", $triangleSide3);
if ($triangleSide3 < 1) {
    exit('Length cant be less then 1');
}

// Обчислюємо площу по добутку катетів

// Знаходимо менший катет
$minCatech = $triangleSide1;
    if ($minCatech > $triangleSide2) {
        $minCatech = $triangleSide2;
    }
    if ($minCatech > $triangleSide3) {
        $minCatech = $triangleSide3;
    }
    echo "First catech is: $minCatech\n";

// Знаходимо гіпотенузу
$hypotenuse = $triangleSide1;
    if ($hypotenuse < $triangleSide2) {
        $hypotenuse = $triangleSide2;
    }
    if ($hypotenuse < $triangleSide3) {
        $hypotenuse = $triangleSide3;
    }
echo "Hypotenuse is: $hypotenuse \n";

// Знаходимо другий катет з периметру
$perimeter = $triangleSide1 + $triangleSide2 + $triangleSide3;

$secondCatech = $perimeter - $minCatech - $hypotenuse;
echo "Second catech is: $secondCatech \n";

// Обчислюємо площу по двом катетам
$triangleAreaCat = ($minCatech * $secondCatech) / 2;
echo "Triangle area is equal $triangleAreaCat using two catechs.\n";


