<?php

/*
 * Написати консольний скрипт, який запитує у користувача кількість членів його 
 * родини, а далі пропонує ввести для кожного члена наступну інформацію:
 * ім’я;
 * стать;
 * вік.
 * 
 * При цьому слід перевіряти валідність введених даних. Так, довжина імені 
 * користувача не повинна бути меншою 3 символів, а вік повинен перебувати в 
 * межах від 1 до 130 років.
 * 
 * Після введення інформації про всіх рідних потрібно вивести на екран наступні 
 * дані:
 * 
 * середній вік рідні;
 * ім’я та вік наймолодшого члена родини;
 * ім’я та вік найстаршого члена родини;
 * кількість дітей віком до 18 років;
 * кількість людей пенсійного віку (пенсійний вік для жінок - 55 років, 
 *      для чоловіків - 60 років).

 */

$MAN_SEX_CODE = 1;
$WOMAN_SEX_CODE = 2;
$RETIREMENT_AGE_MAN = 60;
$RETIREMENT_AGE_WOMAN = 55;
        
echo "Введіть кількість членів родини: ";
fscanf(STDIN, "%d", $familyCount);

if ($familyCount < 1) {
    exit("Кількість членів родини не може бути менше 1!");
}

// Зчитуємо імена, вік та стать і перевіряємо введені дані
$family = array();

for ($i = 0; $i < $familyCount; $i++) {
    $familyMemberNumber = $i + 1;

    $familyMember = array();

    $familyMember["name"] = readline("Введіть імя $familyMemberNumber-го члена родини: ");

    if (strlen($familyMember["name"]) < 3) {
        exit("Довжина імені не може бути менша трьох символів!\n");
    }

    $familyMember["age"] = readline("Введіть вік $familyMemberNumber-го члена родини: ");

    if ($familyMember["age"] < 1 || $familyMember["age"] > 130) {
        exit("Вік не може бути менше 1 чи більше 130!");
    }

    echo "Введіть стать $familyMemberNumber-го члена родини ("
            . "$MAN_SEX_CODE - для чоловіків,"
            . " $WOMAN_SEX_CODE - для жінок): ";
    fscanf(STDIN, "%d", $familyMember["sex"]);
    
    if ($familyMember["sex"] != $MAN_SEX_CODE && $familyMember["sex"] != $WOMAN_SEX_CODE) {
        exit("Неправильно введена стать! Доступні варіанти: "
                . "$MAN_SEX_CODE - чоловік, "
                . "$WOMAN_SEX_CODE - жінка!\n");
    }

    $family[$i] = $familyMember;

    echo "---\n";
}

// Визначаємо середній вік рідні;
$summaryAge;

for ($i = 0; $i < $familyCount; $i++) {
    $summaryAge += $family[$i]['age'];
}

$averageAge = $summaryAge / $familyCount;

echo "Середній вік родини: $averageAge\n";

// Визначаємо ім’я та вік найстаршого та наймолодшого члена родини;
$oldestMember = $family[0];
$youngestMember = $family[0];

for ($i = 1; $i < $familyCount; $i++) {
    if ($family[$i]["age"] > $oldestMember["age"]) {
        $oldestMember = $family[$i];
    }

    if ($family[$i]["age"] < $youngestMember["age"]) {
        $youngestMember = $family[i];
    }
}

echo "Наймолодший член родини - {$youngestMember["name"]} віком "
 . "{$youngestMember["age"]} роки(ів).\n";
 echo "Найстарший член родини - {$oldestMember["name"]} віком "
 . "{$oldestMember["age"]} роки(ів).\n";

// Визначаємо кількість дітей віком до 18 років;
$childrenCount = 0;

for ($i = 0; $i < $familyCount; $i++) {
    if ($family[$i]["age"] < 18) {
        $childrenCount ++;
    }
}

echo "Кількість членів родини віком менше 18 років - $childrenCount.\n";

// Визначємо кількість людей пенсійного віку (пенсійний вік для жінок - 55 років, 
//      для чоловіків - 60 років).
$pensionerCount = 0;

for ($i = 0; $i < $familyCount; $i++) {
    if ($family[$i]["sex"] == $MAN_SEX_CODE && $family[$i]["age"] >= $RETIREMENT_AGE_MAN
            || $family[$i]["sex"] == $WOMAN_SEX_CODE && $family[$i]["age"] >= $RETIREMENT_AGE_WOMAN) {
        $pensionerCount ++;
    }
}

echo "Кількість пенсіонерів - $pensionerCount.\n";

