<?php

/* 
 * Написати консольний скрипт, який запитує у користувача кількість студентів у
 *  групі, а також наступну інформацію для кожного студента:
 * ПІБ;
 * форму навчання (бюджетна чи за контрактом);
 * оцінки із 4 предметів (вища математика, фізика, програмування, креслення) 
 * за 12-бальною шкалою;
 *
 * Після завершення введення даних на екран потрібно вивести список студентів, 
 * які отримають стипендію, та їх середній бал. Даний список повинен бути 
 * відсортованим за спаданням середнього балу. На стипендію можуть претендувати 
 * лише студенти бюджетної форми навчання. Прохідним слід вважати середній бал 
 * вищий або рівний 10.5.
 */

// Запитуємо кількість студенті
$STUDY_FORM_CODE_BUDGET = 1;
$STUDY_FORM_CODE_CONTRACT = 2;
$GRADE_POINT_MAX = 12;
$GRADE_POINT_MIN = 1;

echo "Введіть кількість студентів: ";
fscanf(STDIN, "%d", $studentsCount);

if ($studentsCount < 1) {
    die("Кількість студентів не може бути менше 1!\n");
}

$students = array();
$subjects = array("математика", "фізика", "програмування", "креслення");
$subjectsCount = count($subjects);

// Запитуємо ПІБ
for ($i = 0; $i < $studentsCount; $i++) {
    $student = array();
    $currentStudentNumber = $i + 1;
    $student["name"] = readline("Введіть ім'я $currentStudentNumber-го студента: ");

    // Запитуємо форму навчання
    echo "Введіть форму навчання $currentStudentNumber-го студента, "
            . "$STUDY_FORM_CODE_BUDGET - для бюджету, "
            . "$STUDY_FORM_CODE_CONTRACT - для контракту: ";
    fscanf(STDIN, "%d", $student["studyForm"]);
    
    if($student["studyForm"]!= $STUDY_FORM_CODE_CONTRACT and 
            $student["studyForm"] != $STUDY_FORM_CODE_BUDGET) {
        die("Введено неправильний варіант форми навчання! Потрібно ввести"
                . "$STUDY_FORM_CODE_BUDGET - для бюджету, "
            . "$STUDY_FORM_CODE_CONTRACT - для контракту.\n");
    }

// Запитуємо оцінки з вищої математики, фізики, програмування та креслення
    $student["gradePoints"] = array();
    
    for ($j = 0; $j < $subjectsCount; $j++) {
        echo "Введіть оцінку з предмету "
                . "$subjects[$j] від $GRADE_POINT_MIN до $GRADE_POINT_MAX: ";
        fscanf(STDIN, "%d", $student["gradePoints"][$j]);

        if ($student["gradePoints"][$j] < $GRADE_POINT_MIN or
                $student["gradePoints"][$j] > $GRADE_POINT_MAX) {
            die("Оцінка не може бути менше $GRADE_POINT_MIN чи більше "
                    . "$GRADE_POINT_MAX!\n");
        }
    }
        
    $students[$i] = $student;
}

// Рахуємо середній бал кожного студента
for ($i = 0; $i < $studentsCount; $i++) {
    $gradePointSum = 0;
    
    for ($subjectIndex = 0; $subjectIndex < $subjectsCount; $subjectIndex ++) {
        $gradePointSum += $students[$i]["gradePoints"][$subjectIndex];
    }
    
    $students[$i]["gradePointAverage"] = $gradePointSum / $subjectsCount;
}

// Відбираємо студентів бюджетної форми з середнім балом вище або рівним 10.5
$scholarsCount = 0;
$scholars = array();

for($i = 0; $i < $studentsCount; $i++) {
    if($students[$i]["studyForm"] == $STUDY_FORM_CODE_BUDGET and 
            $students[$i]["gradePointAverage"] >= 10.5) {
        $scholars[$scholarsCount] = $students[$i];
        $scholarsCount++;
    }
}

// Сортуємо відібраних студентів за спаданням середнього балу
$sortFlag = true;

while ($sortFlag) {
    $sortFlag = false;
    
    for ($i = 0; $i < $scholarsCount - 1; $i++) {
        if ($scholars[$i]["gradePointAverage"] < $scholars[$i + 1]["gradePointAverage"]) {
            $tmp = $scholars[$i];
            $scholars[$i] = $scholars[$i + 1];
            $scholars[$i + 1] = $tmp;

            $sortFlag = true;
        }
    }
}

// Виводимо отриманий список
echo "Список студентів, які отримують стипендію:\n";

for ($i = 0; $i < $scholarsCount; $i++) {
    echo "Ім'я студента: {$scholars[$i]["name"]}, середній бал : "
    . "{$scholars[$i]["gradePointAverage"]}.\n";
}