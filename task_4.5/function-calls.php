<?php

/* 
 * Написати функцію, яка повертає кількість власних викликів станом на момент її
 * поточного запуску. Так, при першому виклику вона повинна повернути значення 
 * “1”, при другому - “2”, і так далі.
 */

countFuncCalls();
countFuncCalls();
countFuncCalls();
echo countFuncCalls();

function countFuncCalls() {
    static $callsCount = 0;
    $callsCount++;
    return $callsCount;
}
