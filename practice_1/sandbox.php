<?php

/* Завдання 3.3
 * Написати консольний скрипт, який запитує у користувача кількість студентів у групі, 
 * а також наступну інформацію для кожного студента:
 * ПІБ;
 * форму навчання (бюджетна чи за контрактом);
 * оцінки із 4 предметів (вища математика, фізика, програмування, креслення) за 12-бальною шкалою;

 * Після завершення введення даних на екран потрібно вивести список студентів, 
 * які отримають стипендію, та їх середній бал. Даний список повинен бути відсортованим 
 * за спаданням середнього балу. На стипендію можуть претендувати лише студенти бюджетної форми навчання.
 * Прохідним слід вважати середній бал вищий або рівний 10.5.

 * Для вирішення даної задачі заборонено користуватись стандартними функціями PHP для сортування масивів. */

$formTrainingBudget = 1;
$formTrainingContract = 2;
$ratingMax = 12;
$ratingMin = 1;

// Запитуємо у користувача кількість студентів у групі
$numberStudents = readline("Введіть кількість студентів у групі: ");

if ($numberStudents <= 0) {
    exit("Кількість студентів у групі не може бути менше 1-го.\n");
}

// Запитуємо інформацію про студента:
//ПІБ;
//форму навчання (бюджетна чи за контрактом);
//оцінки із 4 предметів (вища математика, фізика, програмування, креслення) за 12-бальною шкалою;
$arrayStudents = array();

for ($i = 1; $i <= $numberStudents; $i++) {
    $oneStudent = array();

    $oneStudent["name"] = readline("Введіть ПІБ $i -го студента: ");

    echo "Введіть форму навчання $i-го студента ($formTrainingBudget - бюджетна, $formTrainingContract - за контрактом): ";
    fscanf(STDIN, "%d", $formTraining);
    $oneStudent["formTraining"] = $formTraining;

    if ($oneStudent["formTraining"] != $formTrainingBudget && $oneStudent["formTraining"] != $formTrainingContract) {
        exit("Не вірно обрана форма навчання: $formTrainingBudget - бюджетна, $formTrainingContract - за контрактом!\n");
    }

    $oneStudent["estimates"] = array();
    $oneStudent["estimates"]["higher_mathematics"] = readline("Введіть оцінку з математики: ");

    // Перевіряємо правильність введення даних
    
    if ($oneStudent["estimates"]["higher_mathematics"] < $ratingMin || $oneStudent["estimates"]["higher_mathematics"] > $ratingMax) {
        exit("Оцінка не може перевищувати $ratingMax, чи бути меншою $ratingMin.\n");
    }

    $oneStudent["estimates"]["physics"] = readline("Введіть оцінку з фізики: ");

    // Перевіряємо правильність введення даних
    
    if ($oneStudent["estimates"]["physics"] < $ratingMin || $oneStudent["estimates"]["physics"] > $ratingMax) {
        exit("Оцінка не може перевищувати $ratingMax, чи бути меншою $ratingMin.\n");
    }

    $oneStudent["estimates"]["programming"] = readline("Введіть оцінку з програмування: ");

    // Перевіряємо правильність введення даних
    
    if ($oneStudent["estimates"]["programming"] < $ratingMin || $oneStudent["estimates"]["programming"] > $ratingMax) {
        exit("Оцінка не може перевищувати $ratingMax, чи бути меншою $ratingMin.\n");
    }

    $oneStudent["estimates"]["drawing"] = readline("Введіть оцінку з креслення: ");

    // Перевіряємо правильність введення даних
    
    if ($oneStudent["estimates"]["drawing"] < $ratingMin || $oneStudent["estimates"]["drawing"] > $ratingMax) {
        exit("Оцінка не може перевищувати $ratingMax, чи бути меншою $ratingMin.\n");
    }

    // Додаємо середній бал студента
    $generalStudentPoint = 0;
    $countRatings = count($oneStudent["estimates"]);

    foreach ($oneStudent["estimates"] as $oneRating) {
        $generalStudentPoint += $oneRating;
    }

    $oneStudent["average_score"] = $generalStudentPoint / $countRatings;

    $arrayStudents[] = $oneStudent;

    echo "----------------\n";
}

// Відсортовуємо тих студентів, хто претендує на стипендію
$fellow = array();

for ($i = 0; $i < count($arrayStudents); $i++) {
    if ($arrayStudents[$i]["formTraining"] == 1 && $arrayStudents[$i]["average_score"] >= 10.5) {
        $fellow[] = $arrayStudents[$i];
    }
}

// Сортуємо список за спаданням середнього балу
$sortFlag = true;

while ($sortFlag) {
    $sortFlag = false;

    for ($i = 0; $i < count($fellow) - 1; $i++) {
        if ($fellow[$i]["average_score"] < $fellow[$i + 1]["average_score"]) {
            $tmp = $fellow[$i];
            $fellow[$i] = $fellow[$i + 1];
            $fellow[$i + 1] = $tmp;

            $sortFlag = true;
        }
    }
}

// Виводимо список студентів, які отримають стипендію, та їх середній бал.

echo "Список студентів, які отримають стипендію, та їх середній бал:\n";
for ($i = 0; $i < count($fellow); $i++) {
    echo "- Ім'я: {$fellow[$i]["name"]}. Середній бал: {$fellow[$i]["average_score"]}\n.";
}