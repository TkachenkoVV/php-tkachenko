<?php

/* Написати скріпт який приймає від користувача кількість працівників, їх ім'я, стать, вік.
 * Виводить на екран список чоловіків та список жінок віком старше 21 року. */

// Зчитуємо кількість працівників
echo "Введіть кількість працівників: ";
fscanf(STDIN, "%d", $workersCount);

// Зчитуємо імена та вік
$workers = array();

for ($i = 0; $i < $workersCount; $i++) {
    $workerNumber = $i + 1;

    $worker = array();

    $worker["name"] = readline("Введіть ім'я $workerNumber-го працівника: ");
    $worker["age"] = readline("Введіть вік $workerNumber-го працівника: ");
    $worker["sex"] = readline("Введіть стать $workerNumber-го працівника (1 - чоловік, 2 - жінка)");
    
    if ($worker["sex"] != 1 && $worker["sex"] != 2) {
        exit("Неправильно введена стать!");
    }
    
    $workers[$i] = $worker;

    echo "---\n";
}

// Отримуємо масиви чоловіків та жінок які відповідають критерію
$women = array();
$men = array();
for ($i = 0; $i < $workersCount; $i ++) {
    if($workers[$i]["age"] > 21) {
        if ($workers[$i]["sex"] == 1) {
            $men[] = $workers[$i];
        }
        else {
            $woman[] = $workers[$i];
        }
    }
}
// Виводимо список чоловіків
echo "Список чоловіків, вік яких більше 21 року:\n";

for($i = 0; $i < count($men); $i++) {
    echo "- {$men["name"]} ({$men["age"]})";
}

// Виводимо список жінок
for($i = 0; $i < count($men); $i++) {
    echo "- {$women["name"]} ({$women[  "age"]})";
}