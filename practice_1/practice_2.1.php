<?php

/*
 * Написати консольний скрипт, який приймає від користувача 3 цілих невід’ємних
 * числа і виводить на екран найбільше із них. У випадку, якщо користувач 
 * вводить некоректне значення, потрібно виводити інформацію про помилку.
 */

//Зчитати

echo "Pls enter first number: ";
fscanf(STDIN, "%d", $number1);

$errNotPositive = "You need to enter positive integer!\n";

if($number1 < 0) {
    exit ($errNotPositive);
}

echo "Pls enter second number: ";
fscanf(STDIN, "%d", $number2);

if($number2 < 0) {
    exit ($errNotPositive);
}

echo "Pls enter third number: ";
fscanf(STDIN, "%d", $number3);

if($number2 < 0) {
    exit ($errNotPositive);
}

//Порівняти

$maximum = $number1;

if ($maximum < $number2) {
    $maximum = $number2;
}

if ($maximum < $number3) {
    $maximum = $number3;
}

//Вивести
echo "The maximum is: $maximum\n";