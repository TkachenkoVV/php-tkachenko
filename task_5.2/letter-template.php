<?php

/*
 * Написати функцію, яка приймає шаблон тексту електронного листа із
 * підстановочними змінними, асоціативний масив їх значень і повертає текст теми
 * та тіла листа із заміненими підстановочними змінними.
 */

$mailTemplate = array(
    'subject' => "Завершився термін дії пакету {{\$packageName}}",
    'body' => "Доброго дня, {{\$userName}},\n\n"
    . "Термін дії Вашого пакету послуг {{\$packageName}} на порталі ibud.ua "
    . "завершився. Будь ласка, замовте новий пакет послуг, скориставшись формою "
    . "замовлення пакету, яку можна знайти за посиланням {{\$orderFormURL}}. "
    . "Якщо у Вас є питання, будь ласка, зв’яжіться із Вашим персональним "
    . "консультантом.\n\nЗ повагою,\nКоманда ibud.ua"
);

$substitutionVars = array (
    "userName" => "Гунько Борис Володимирович",
    "packageName" => "Стандарт 12 місяців 25000",
    "orderFormURL" => "https://ibud.ua/ru/advertising-packages"
);

$mail = substituteVars($mailTemplate, $substitutionVars);
echo "Тема: {$mail['subject']}\n";
echo "Тіло: {$mail['body']}\n";

function substituteVars($template, $vars) {
    $message = $template;

    foreach ($vars as $varName => $value) {
        $message['subject'] = str_replace('{{$'.$varName.'}}', $value, $message['subject']);
        $message['body'] = str_replace('{{$'.$varName.'}}', $value, $message['body']);
    }

    return $message;
}
