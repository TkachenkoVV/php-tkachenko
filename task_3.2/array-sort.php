<?php

/*
 * Написати консольний скрипт, який запитує у користувача розмір масиву, 
 * пропонує ввести значення його елементів та режим їх сортування (за зростанням
 *  або спаданням). При цьому в якості елементів дозволяється вводити лише 
 * числові значення (як додатні, так і від’ємні). По завершенню роботи скрипт 
 * повинен вивести на екран елементи масиву, відсортовані у вибраному режимі.
 */

$SORT_CODE_ASC = 1;
$SORT_CODE_DESC = 0;

//Зчитуємо розмір масиву
echo "Введіть розмір масиву: ";
fscanf(STDIN, "%d", $arraySize);

if ($arraySize == 0) {
    exit("Розмір масиву не може дорівнювати нулю!\n");
}

//Зчитуємо елементи масиву
$array = array();

for ($i = 0; $i < $arraySize; $i++) {
    $arrayPos = $i + 1;
    $array[$i] = readline("Введіть $arrayPos-й елемент масиву: ");
}

//Зчитуємо порядок сортування
echo("Введіть порядок сортування масиву $SORT_CODE_ASC - зростання, $SORT_CODE_DESC - спадання: ");
fscanf(STDIN, "%d", $sortOrder);

if ($sortOrder != $SORT_CODE_ASC && $sortOrder != $SORT_CODE_DESC) {
    exit("Неправильно введений порядок сортування масиву, "
            . "$SORT_CODE_ASC - зростання, "
            . "$SORT_CODE_DESC - спадання!\n");
}
// Сортуємо масив в залежності від  змінної
$sortFlag = TRUE;

if ($sortOrder == $SORT_CODE_DESC) {
    while ($sortFlag) {
        $sortFlag = FALSE;

        for ($i = 0; $i < $arraySize - 1; $i++) {

            if ($array[$i + 1] < $array[$i]) {
                $temp = $array[$i + 1];
                $array[$i + 1] = $array[$i];
                $array[$i] = $temp;
                $sortFlag = TRUE;
            }
        }
    }
}

else {
    while ($sortFlag) {
        $sortFlag = FALSE;

        for ($i = 0; $i < $arraySize - 1; $i++) {

            if ($array[$i + 1] > $array[$i]) {
                $temp = $array[$i + 1];
                $array[$i + 1] = $array[$i];
                $array[$i] = $temp;
                $sortFlag = TRUE;
            }
        }
    }
}

//Виводимо елементи масиву
print_r($array);
